/*
  Copyright (c) 20016-2023 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <catch.hpp>

#include "array_ptr.h"

namespace fsm {

  TEST_CASE("array handling", "[array_ptr]") {
    SECTION("default initialization") {
      constexpr ArrayPtr<int> ptr {};
      REQUIRE(ptr.length() == 0);

      for ([[maybe_unused]] auto item : ptr) {
        FAIL("Array is empty");
      }
    }

    SECTION("from simple array") {
      int array[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
      const ArrayPtr<int> ptr { array };
      REQUIRE(ptr.length() == 10);

      auto counter = 0;
      for (auto item : ptr) {
        REQUIRE(counter == item);
        ++counter;
      }

      REQUIRE(counter == 10);
      REQUIRE(ptr[3] == 3);
    }

    SECTION("from pointer and length") {
      int array[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
      const ArrayPtr<int> ptr { array, 4 };
      REQUIRE(ptr.length() == 4);

      auto counter = 0;
      for (auto item : ptr) {
        REQUIRE(counter == item);
        ++counter;
      }

      REQUIRE(counter == 4);
    }

    SECTION("from std::array") {
      std::array<int, 10> array { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
      const ArrayPtr<int> ptr { array };
      REQUIRE(ptr.length() == 10);

      auto counter = 0;
      for (auto item : ptr) {
        REQUIRE(counter == item);
        ++counter;
      }

      REQUIRE(counter == 10);
    }

    SECTION("from std::array modify") {
      std::array<int, 10> array { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
      ArrayPtr<int> ptr { array };
      REQUIRE(ptr.length() == 10);

      auto counter = 0;
      for (auto& item : ptr) {
        REQUIRE(counter == item);
        item = 42;
        ++counter;
      }

      REQUIRE(counter == 10);

      const ArrayPtr ptr2 { ptr };
      for (auto& item : ptr2) {
        REQUIRE(item == 42);
      }
    }
  }
}
