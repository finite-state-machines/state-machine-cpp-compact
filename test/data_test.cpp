/*
  Copyright (c) 20016-2023 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <catch.hpp>

#include "data.h"

namespace fsm {
  TEST_CASE("State", "[State]") {
    SECTION("initialization invalid") {
      constexpr State state {};
      REQUIRE(state.Id() == -101);
      REQUIRE(state == kInvalidState);
    }

    SECTION("initialization valid") {
      constexpr State state { 1 };
      REQUIRE(state.Id() == 1);
    }

    SECTION("initial state") {
      constexpr auto state = State::GetInitialState();
      REQUIRE(state.Id() == -1);
      REQUIRE(state == kInitialState);
    }

    SECTION("final state") {
      constexpr auto state = State::GetFinalState();
      REQUIRE(state.Id() == -2);
      REQUIRE(state == kFinalState);
    }
  }

  TEST_CASE("Event", "[Event]") {
    SECTION("initialization default") {
      constexpr Event event {};
      REQUIRE(event.Id() == -1);
      REQUIRE(event == kNoEvent);
    }

    SECTION("initialization custom") {
      constexpr Event event { 1 };
      REQUIRE(event.Id() == 1);
    }

    SECTION("start event") {
      constexpr auto event = Event::GetStartEvent();
      REQUIRE(event.Id() == -2);
      REQUIRE(event == kStartEvent);
    }
  }

  TEST_CASE("History", "[History]") {
    SECTION("initialization default") {
      constexpr History history {};
      REQUIRE_FALSE(history.IsHistory());
      REQUIRE_FALSE(history.IsDeepHistory());
    }

    SECTION("history") {
      constexpr auto history = History::GetHistory();
      REQUIRE(history.IsHistory());
      REQUIRE_FALSE(history.IsDeepHistory());
    }

    SECTION("deep history") {
      constexpr auto history = History::GetDeepHistory();
      REQUIRE_FALSE(history.IsHistory());
      REQUIRE(history.IsDeepHistory());
    }
  }

  TEST_CASE("Not", "[Not]") {
    SECTION("simple function pointer") {
      constexpr auto func = [](const bool a) { return a; };
      REQUIRE_FALSE(func(false));
      REQUIRE(Not(func)(false));
    }

    SECTION("simple lambda") {
      REQUIRE(Not(([](const bool a) { return a; }))(false));
      REQUIRE_FALSE(Not(([](const bool a) { return a; }))(true));
    }

    SECTION("lambda and functor") {
      constexpr auto value = 42;
      const auto helper = Not([value](const bool a) { return a && value == 42; });
      REQUIRE(helper(false));
      REQUIRE(Not(helper)(true));
      REQUIRE_FALSE(Not(helper)(false));
    }
  }

  TEST_CASE("EndPoint", "[EndPoint]") {
    SECTION("no history") {
      constexpr EndPoint end_point { kFinalState };
      REQUIRE(end_point.to == kFinalState);
      REQUIRE_FALSE(end_point.history.IsHistory());
      REQUIRE_FALSE(end_point.history.IsDeepHistory());
    }

    SECTION("history") {
      constexpr EndPoint end_point { kFinalState, History::GetHistory() };
      REQUIRE(end_point.to == kFinalState);
      REQUIRE(end_point.history.IsHistory());
      REQUIRE_FALSE(end_point.history.IsDeepHistory());
    }

    SECTION("history short") {
      constexpr auto end_point = H(kFinalState);
      REQUIRE(end_point.to == kFinalState);
      REQUIRE(end_point.history.IsHistory());
      REQUIRE_FALSE(end_point.history.IsDeepHistory());
    }

    SECTION("deep history") {
      constexpr EndPoint end_point { kFinalState, History::GetDeepHistory() };
      REQUIRE(end_point.to == kFinalState);
      REQUIRE_FALSE(end_point.history.IsHistory());
      REQUIRE(end_point.history.IsDeepHistory());
    }

    SECTION("deep history short") {
      constexpr auto end_point { Hx(kFinalState) };
      REQUIRE(end_point.to == kFinalState);
      REQUIRE_FALSE(end_point.history.IsHistory());
      REQUIRE(end_point.history.IsDeepHistory());
    }
  }
}