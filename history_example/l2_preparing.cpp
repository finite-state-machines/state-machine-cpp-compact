#include "l2_preparing.h"

#include <iostream>

#include "events.h"

using namespace fsm;

L2Preparing::L2Preparing()
    // clang-format off
    : machine_{"L2-Preparing"},
      transitions_{
        Transition<int>{kInitialState, kStartEvent, kL3P1}, Transition<int>{kL3P1, kNext, kL3P2},
        Transition<int>{kL3P2, kNext, kL3P3}, Transition<int>{kL3P3, kNext, kL3P4},
        Transition<int>{kL3P4, kNext, kFinalState}
      },
      entry_actions_{
        Action<int>{kL3P1, [](int) { std::cout << "starting P1\n"; }},
        Action<int>{kL3P2, [](int) { std::cout << "starting P2\n"; }},
        Action<int>{kL3P3, [](int) { std::cout << "starting P3\n"; }},
        Action<int>{kL3P4, [](int) { std::cout << "starting P4\n"; }},
          // clang-format on
      } {
  machine_.Transitions(transitions_).EntryActions(entry_actions_);
}
