#ifndef L2_PREPARING_H
#define L2_PREPARING_H

#include "fsm.h"

class L2Preparing {
  fsm::Fsm<int> machine_;

  static constexpr fsm::State kL3P1{311};
  static constexpr fsm::State kL3P2{312};
  static constexpr fsm::State kL3P3{313};
  static constexpr fsm::State kL3P4{314};

  fsm::Transition<int> transitions_[5];
  fsm::Action<int> entry_actions_[4];

 public:
  L2Preparing();

  constexpr fsm::Fsm<int> &Fsm() {
    return machine_;
  }
};

#endif  // L2_PREPARING_H
