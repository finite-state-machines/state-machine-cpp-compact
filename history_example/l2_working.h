#ifndef L2_WORKING_H
#define L2_WORKING_H

#include "fsm.h"

class L2Working {
  fsm::Fsm<int> machine_;

  static constexpr fsm::State kL3W1{321};
  static constexpr fsm::State kL3W2{322};
  static constexpr fsm::State kL3W3{323};
  static constexpr fsm::State kL3W4{324};
  static constexpr fsm::State kL3W5{325};

  fsm::Transition<int> transitions_[6];
  fsm::Action<int> entry_actions_[5];
  fsm::Action<int> do_actions_[1];

 public:
  L2Working();

  constexpr fsm::Fsm<int> &Fsm() {
    return machine_;
  }
};

#endif  // L2_WORKING_H
