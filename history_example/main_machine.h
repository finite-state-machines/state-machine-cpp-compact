#ifndef MAIN_MACHINE_H
#define MAIN_MACHINE_H

#include "working.h"

class MainMachine {
  Working fsm_working_;

  fsm::Fsm<int> machine_;

  static constexpr fsm::State kInitializing{1};
  static constexpr fsm::State kWorking{2};
  static constexpr fsm::State kHandlingError{3};
  static constexpr fsm::State kFinalizing{4};

  fsm::Transition<int> transitions_[10];
  fsm::Child<int> children_[1];
  fsm::Action<int> entry_actions_[4];
  fsm::Action<int> do_actions_[1];
  fsm::Action<int> exit_actions_[1];

 public:
  MainMachine();
  fsm::Fsm<int>& GetMachine() {
    return machine_;
  }

  void Start();
  void DumpStates() const;
};

#endif  // MAIN_MACHINE_H
