#include <iostream>
#include <map>
#include <string>

#include "events.h"
#include "main_machine.h"

using namespace fsm;

const std::map<char, Event> kEvents{
    {'B', kBreak}, {'C', kContinue}, {'D', kContinueDeep}, {'N', kNext}, {'R', kRestart},
    {'b', kBreak}, {'c', kContinue}, {'d', kContinueDeep}, {'n', kNext}, {'r', kRestart},
};

void Process(const char key, MainMachine& main_machine) {
  const auto it = kEvents.find(key);
  if (it != kEvents.end()) {
    main_machine.GetMachine().Trigger(it->second, 42);
    main_machine.DumpStates();
  }

  if (key == '#') {
    main_machine.GetMachine().Do(23);
  }
}

char GetSingleCharacter() {
  std::string line;
  std::getline(std::cin, line);
  return line.empty() ? 'n' : line[0];
}

void Run() {
  try {
    MainMachine main_machine;

    main_machine.Start();

    while (true) {
      const auto input = GetSingleCharacter();
      if (input == 'q' || input == 'Q') {
        break;
      }

      Process(input, main_machine);
    }

    std::cout << "The end!\n";
  } catch (...) {
  }
}

int main() noexcept {
  Run();
}
