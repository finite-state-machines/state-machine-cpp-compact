#include "main_machine.h"

#include <iostream>

#include "events.h"

using namespace fsm;

MainMachine::MainMachine()
    // clang-format off
    : machine_{"Main"},
      transitions_{
        Transition<int>{kInitialState, kStartEvent, kInitializing},
        Transition<int>{kInitializing, kNext, kWorking},
        Transition<int>{kWorking, kBreak, kHandlingError},
        Transition<int>{kWorking, kNoEvent, kFinalizing},
        Transition<int>{kHandlingError, kRestart, kWorking},
        Transition<int>{kHandlingError, kContinue, H(kWorking)},
        Transition<int>{kHandlingError, kContinueDeep, Hx(kWorking)},
        Transition<int>{kHandlingError, kNext, kFinalizing},
        Transition<int>{kHandlingError, kBreak, kFinalState},
        Transition<int>{kFinalizing, kNext, kInitializing}
      },
      children_{
        Child<int>{kWorking, fsm_working_.Fsm()}
      },
      entry_actions_{
        Action<int>{kInitializing, [](int) { std::cout << "starting Initializing\n"; }},
        Action<int>{kWorking, [](int) { std::cout << "starting Working\n"; }},
        Action<int>{kHandlingError, [](int) { std::cout << "starting HandlingError\n"; }},
        Action<int>{kFinalizing, [](int) { std::cout << "starting Finalizing\n"; }}},
      do_actions_{
        Action<int>{kWorking, [](int) { std::cout << "doing Working\n"; }},
      },
      exit_actions_{
        Action<int>{kHandlingError, [](int) { std::cout << "leaving HandlingError\n"; }},
          // clang-format on
      } {
  machine_.Transitions(transitions_)
      .Children(children_)
      .EntryActions(entry_actions_)
      .DoActions(do_actions_)
      .ExitActions(exit_actions_);
}

void MainMachine::Start() {
  machine_.Start(0);
}

void MainMachine::DumpStates() const {
  machine_.Dump([](const int level, const char* name, const int stateId) {
    std::cout << name << "(" << level << ")"
              << ": " << stateId << " - ";
  });
  std::cout << std::endl;
}
