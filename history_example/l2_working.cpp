#include "l2_working.h"

#include <iostream>

#include "events.h"

using namespace fsm;

L2Working::L2Working()
    // clang-format off
    : machine_{"L2-Working"},
      transitions_{
        Transition<int>{kInitialState, kStartEvent, kL3W1},
        Transition<int>{kL3W1, kNext, kL3W2},
        Transition<int>{kL3W2, kNext, kL3W3},
        Transition<int>{kL3W3, kNext, kL3W4},
        Transition<int>{kL3W4, kNext, kL3W5},
        Transition<int>{kL3W5, kNext, kFinalState}
      },
      entry_actions_{
        Action<int>{kL3W1, [](int) { std::cout << "starting W1\n"; }},
        Action<int>{kL3W2, [](int) { std::cout << "starting W2\n"; }},
        Action<int>{kL3W3, [](int) { std::cout << "starting W3\n"; }},
        Action<int>{kL3W4, [](int) { std::cout << "starting W4\n"; }},
        Action<int>{kL3W5, [](int) { std::cout << "starting W5\n"; }}
      },
      do_actions_{
        Action<int>{kL3W3, [](int) { std::cout << "doing W3\n"; }
      },
          // clang-format on
      } {
  machine_.Transitions(transitions_).EntryActions(entry_actions_).DoActions(do_actions_);
}
