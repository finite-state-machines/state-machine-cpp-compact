#ifndef WORKING_H
#define WORKING_H

#include "l2_preparing.h"
#include "l2_working.h"

class Working {
  L2Preparing preparing_;
  L2Working working_;
  fsm::Fsm<int> machine_;

  static constexpr fsm::State kL2Initializing{21};
  static constexpr fsm::State kL2Preparing{22};
  static constexpr fsm::State kL2Working{23};

  fsm::Transition<int> transitions_[4];
  fsm::Child<int> children_[2];
  fsm::Action<int> entry_actions_[3];
  fsm::Action<int> do_actions_[1];

 public:
  Working();

  constexpr fsm::Fsm<int> &Fsm() {
    return machine_;
  }
};

#endif  // WORKING_H
