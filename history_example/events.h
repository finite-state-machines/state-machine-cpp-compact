#ifndef EVENTS_H
#define EVENTS_H

// the events used by the state machines
#include "data.h"

constexpr fsm::Event kBreak{'b'};
constexpr fsm::Event kContinue{'c'};
constexpr fsm::Event kContinueDeep{'d'};
constexpr fsm::Event kNext{'n'};
constexpr fsm::Event kRestart{'r'};

#endif  // EVENTS_H
