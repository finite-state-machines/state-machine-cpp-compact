#include "working.h"

#include <iostream>

#include "events.h"

using namespace fsm;

Working::Working()
    // clang-format off
    : machine_{"Working"},
      transitions_{
        Transition<int>{kInitialState, kStartEvent, kL2Initializing},
        Transition<int>{kL2Initializing, kNext, kL2Preparing},
        Transition<int>{kL2Preparing, kNoEvent, kL2Working},
        Transition<int>{kL2Working, kNoEvent, kFinalState},
      },
      children_{
        Child<int>{kL2Preparing, preparing_.Fsm()}, Child<int>{kL2Working, working_.Fsm()}
      },
      entry_actions_{
        Action<int>{kL2Initializing, [](int) { std::cout << "starting L2 Initializing\n"; }},
        Action<int>{kL2Preparing, [](int) { std::cout << "starting L2 Preparing\n"; }},
        Action<int>{kL2Working, [](int) { std::cout << "starting L2 Working\n"; }},
      },
      do_actions_{
        Action<int>{kL2Working, [](int) { std::cout << "doing L2 Working\n"; }},
          // clang-format on
      } {
  machine_.Transitions(transitions_).Children(children_).EntryActions(entry_actions_).DoActions(do_actions_);
}
