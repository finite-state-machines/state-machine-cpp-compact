add_executable(history_example "")

target_sources(history_example
    PRIVATE
        main.cpp
        l2_preparing.cpp
        l2_working.cpp
        working.cpp
        main_machine.cpp
        events.h
        l2_preparing.h
        l2_working.h
        working.h
        main_machine.h)
