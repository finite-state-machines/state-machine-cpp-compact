# A ready to use state machine for C++
Copyright &copy; 2020 Frank Listing

There are a lot of variants known to implement state machines. Most of them merge the code of the state machines behavior together with the functional code. A better solution is to strictly separate the code for the state machines logic from the functional code. An existing state machine component is parametrized to define its behavior. 

This help file includes the documentation of an implemantation of a ready to use FSM (Finite State Machine). Using this state machine is very simple. Just create a state machine object and some states, after that define the transitions and the state action.

In opposite to the other C++ state machine at this site, this machine does not use heap and has a smaller footprint. Thats why the configuration is a little bit more complicated than for the other example.

The rest of the decription will follow soon.

***
Copyright &copy; 2020 Frank Listing
