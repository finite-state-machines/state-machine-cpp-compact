/*
  Copyright (c) 20016-2023 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#pragma once

#include <functional>
#include <utility>

namespace fsm {

  /**
   * A class to encapsulate an identifier.
   */
  class IdHolder {
    //! The id of this instance.
    int id_;

  public:
    /**
     * Gets the identifier.
     */
    [[nodiscard]] constexpr int Id() const {
      return id_;
    }

    /**
     * Compares two instances for equality.
     * @param other The instance to compare this instance to.
     * @return Returns true if both instances are equal; false otherwise.
     */
    constexpr bool operator==(const IdHolder& other) const {
      return id_ == other.id_;
    }

    /**
     * Compares two instances for unequality.
     * @param other The instance to compare this instance to.
     * @return Returns true if both instances are not equal; false otherwise.
     */
    constexpr bool operator!=(const IdHolder& other) const {
      return !(*this == other);
    }

  protected:
    /**
     * Initializes a new instance of the @see IdHolder class.
     * @remark Custom states can be positive or null only. For negative values the absolut value is used.
     * @param id The id of the state.
     */
    explicit constexpr IdHolder(const int id) : id_ { id } {}
  };

  /**
   * A class to have a defined state type. Encapsulates an identifier.
   */
  class State final : public IdHolder {
    //! A constant defining the id of the initial pseudo state
    static constexpr int kInitialStateId = -1;

    //! A constant defining the id of the final pseudo state
    static constexpr int kFinalStateId = -2;

    //! A constant defining the id of the invalid state
    static constexpr int kInvalidStateId = -101;

  public:
    /**
     * Initializes a new instance of the @see State class. This default constructor creates an invalid state.
     */
    constexpr State() : IdHolder { kInvalidStateId } {}

    /**
     * Initializes a new instance of the @see State class.
     * @remark Custom states can be positive or null only. For negative values the absolut value is used.
     * @param id The id of the state.
     */
    explicit constexpr State(const int id) : IdHolder { id < 0 ? -id : id } {}

    /**
     * Creates an initial state object.
     */
    constexpr static State GetInitialState() {
      return { kInitialStateId, 42 };
    }

    /**
     * Creates a final state object.
     */
    constexpr static State GetFinalState() {
      return { kFinalStateId, 42 };
    }

  private:
    /**
     * Initializes a new instance of the @see State class.
     * Non public construction to be able to create special states.
     * @param id The id of the state.
     * @param not_used The second parameter is only to be able to call this constructor.
     */
    constexpr State(const int id, int not_used) : IdHolder { id } {}
  };

  /**
   * A class to have a defined event type. Encapsulates an identifier.
   */
  class Event final : public IdHolder {
    //! A constant defining the id of the start event
    static constexpr int kStartEventId = -2;

    //! A constant defining the id of the not existing event
    static constexpr int kNoEventId = -1;

  public:
    /**
     * Initializes a new instance of the @see Event class.
     * @remark Creates the not existing event (used for transitions without a trigger).
     */
    constexpr Event() : IdHolder { kNoEventId } {}

    /**
     * Initializes a new instance of the @see Event class.
     * @remark Custom states can be positive or null only. For negative values the absolut value is used.
     * @param id The id of the state.
     */
    explicit constexpr Event(const int id) : IdHolder { id < 0 ? -id : id } {}

    /**
     * Creates the start event.
     */
    constexpr static Event GetStartEvent() {
      return { kStartEventId, 42 };
    }

  private:
    /**
     * Initializes a new instance of the @see Event class.
     * Non public construction to be able to create special states.
     * @param id The id of the state.
     * @param not_used The second parameter is only to be able to call this constructor.
     */
    constexpr Event(const int id, int not_used) : IdHolder { id } {}
  };

  //! A constant defining the initial pseudo state.
  constexpr State kInitialState = State::GetInitialState();

  //! A constant defining the final pseudo state.
  constexpr State kFinalState = State::GetFinalState();

  //! A constant defining an invalid state.
  constexpr State kInvalidState = State {};

  //! A constant defining the start event.
  constexpr Event kStartEvent = Event::GetStartEvent();

  //! A constant defining the not existing event (used for transitions without a trigger).
  constexpr Event kNoEvent = Event {};

  /**
   * Helper class to have a well defined type to support history.
   */
  class History final {
    /**
     * The possible history types.
     */
    enum class HistoryType : unsigned char {
      kNone = 'N',
      kHistory = 'H',
      kDeepHistory = 'D',
    };

    //! The type of history represented by this object.
    HistoryType history_ { HistoryType::kNone };

  public:
    /**
     * Initializes a new instance of the @see History class (not using history).
     */
    constexpr History() = default;

    /**
     * Gets an instance representing history.
     */
    constexpr static History GetHistory() {
      return History { HistoryType::kHistory };
    }

    /**
     * Gets an instance representing deep history.
     */
    constexpr static History GetDeepHistory() {
      return History { HistoryType::kDeepHistory };
    }

    /**
     * Gets a value indicating whether to use history.
     */
    [[nodiscard]] constexpr bool IsHistory() const {
      return history_ == HistoryType::kHistory;
    }

    /**
     * Gets a value indicating whether to use deep history.
     */
    [[nodiscard]] constexpr bool IsDeepHistory() const {
      return history_ == HistoryType::kDeepHistory;
    }

  private:
    /**
     * Initializes a new instance of the @see History class.
     * @param history The history value to store.
     */
    constexpr explicit History(const HistoryType history) : history_ { history } {}
  };

  /**
   * A functor used to create a condition with a reversed result.
   */
  template<typename TFunc>
  class NotHelper final {
    //! Stores the function pointer from which the result is negated.
    TFunc func_;

  public:
    /**
     * Initializes a new instance of the @see NotHelper class.
     * @param func A function pointer from which the result is negated.
     */
    explicit NotHelper(const TFunc& func) : func_ { func } {}

    /**
     * Calls the stored function pointer and negates it's result.
     */
    template<typename TParam>
    bool operator()(
        TParam&& param) const { // NOLINT(cppcoreguidelines-missing-std-forward) - IDE cannot see the function type
      return !func_(std::forward<TParam>(param));
    }
  };

  /**
   * A function to create a condition with a reversed result.
   * @param condition A condition to check negated for a transition.
   */
  template<typename T>
  NotHelper<T> Not(const T condition) {
    return NotHelper<T> { condition };
  }

  /**
   * A class to store all information about the destination of a transition.
   */
  struct EndPoint final {
    //! The destination state of the transition.
    const State to; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The type of history to use, the default is none.
    const History history {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    // ReSharper disable once CppNonExplicitConvertingConstructor
    /**
     * Initializes a new instance of the @see TransitionEndPoint class.
     * @param state_to The destination state of the transition.
     */
    constexpr EndPoint(const State state_to) : EndPoint { state_to, History {} } {}

    /**
     * Initializes a new instance of the TransitionEndPoint class.
     * @param state_to The destination state of the transition.
     * @param history_type The type of history to use.
     */
    constexpr EndPoint(const State state_to, const History history_type) : to { state_to }, history { history_type } {}
  };

  //! A function to create a history @see EndPoint.
  constexpr EndPoint H(const State state) {
    return { state, History::GetHistory() };
  }

  //! A function to create a deep history @see EndPoint.
  constexpr EndPoint Hx(const State state) {
    return { state, History::GetDeepHistory() };
  }

  /**
   * Class holding all information about a transition.
   * @tparam T The type of data provided to the condition and action handlers.
   */
  template<typename T>
  class Transition {
    //! The start state of this transition.
    const State state_from_ {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The Event that initiates this transition.
    const Event trigger_; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The condition handler of this transition.
    const std::function<bool(T)> condition_; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The destination state of this transition.
    const State state_to_ {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! A value containing the information whether to use history.
    const History history_ {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

  public:
    /**
     * Initializes a new instance of the Transition class.
     * @param state_from The start point of this transition.
     * @param trigger The Event that initiates this transition.
     * @param condition The condition handler of this transition.
     * @param state_to The destination state of this transition.
     * @param history A value containing the information whether to use history.
     */
    constexpr Transition(
        const State state_from,
        const Event trigger,
        std::function<bool(T)> condition,
        const State state_to,
        const History history) :
        state_from_ { state_from },
        trigger_ { trigger },
        condition_ { std::move(condition) },
        state_to_ { state_to },
        history_ { history } {}

    /**
     * Initializes a new instance of the @see Transition class.
     * @param state_from The start point of this transition.
     * @param trigger The Event that initiates this transition.
     * @param condition The condition handler of this transition.
     * @param state_to The destination state of this transition.
     */
    constexpr Transition(
        const State state_from,
        const Event trigger,
        std::function<bool(T)> condition,
        const State state_to) :
        Transition { state_from, trigger, condition, state_to, History() } {}

    /**
     * Initializes a new instance of the Transition class.
     * @param state_from The start point of this transition.
     * @param trigger The Event that initiates this transition.
     * @param state_to The destination state of this transition.
     */
    constexpr Transition(const State state_from, const Event trigger, const State state_to) :
        Transition { state_from, trigger, nullptr, state_to, History() } {}

    /**
     * Initializes a new instance of the Transition class.
     * @param state_from The start point of this transition.
     * @param trigger The Event that initiates this transition.
     * @param end_point The end point of this transition.
     */
    constexpr Transition(const State state_from, const Event trigger, const EndPoint end_point) :
        Transition { state_from, trigger, nullptr, end_point.to, end_point.history } {}

    /**
     * Initializes a new instance of the Transition class.
     * @param state_from The start point of this transition.
     * @param trigger The Event that initiates this transition.
     * @param condition The condition handler of this transition.
     * @param end_point The end point of this transition.
     */
    constexpr Transition(
        const State state_from,
        const Event trigger,
        std::function<bool(T)> condition,
        const EndPoint end_point) :
        Transition { state_from, trigger, condition, end_point.to, end_point.history } {}

    /**
     * Gets a value indicating whether the stored start state and trigger is equal to the
     * state and trigger given as parameter.
     * @param state_from The start point of this transition.
     * @param trigger The value to compare with the stored trigger.
     * @return Returns true is the values (state and trigger) are equal to the stored values; false otherwise.
     */
    [[nodiscard]] bool Match(const State state_from, const Event trigger) const {
      return state_from_ == state_from && trigger_ == trigger;
    }

    /**
     * Executes the condition handler of this transition.
     * @param data The data provided to the condition and action handlers.
     * @returns If there is no condition handler -> returns true. If there is a
     * condition handler, it is called and it's return value is taken.
     */
    [[nodiscard]] bool Condition(T data) const {
      return condition_ == nullptr || condition_(data);
    }

    /**
     * Get the history information.
     * @return Returns the history value stored in this instance.
     */
    [[nodiscard]] const History& GetHistory() const {
      return history_;
    }

    /**
     * Gets the destination state.
     * @return Returns the destination state.
     */
    [[nodiscard]] State To() const {
      return state_to_;
    }

    /**
     * Gets a value indicating whether the transition has the final state as end point.
     * @return Returns true if the final state is the end point; false otherwise.
     */
    [[nodiscard]] bool IsToFinal() const {
      return state_to_ == kFinalState;
    }
  };

  /**
   * A class used to store an action and the owning state.
   */
  template<typename T>
  class Action {
    //! The owning state.
    const State state_; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The associated action.
    const std::function<void(T)> action_; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

  public:
    /**
     * Initializes a new instance of the @see Action class.
     * @param state The state which is the owner of this action.
     * @param action The action function to store.
     */
    constexpr Action(const State state, const std::function<void(T)> action) : state_ { state }, action_ { action } {}

    /**
     * Calls the the stored action, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param data The data provided to the action handler.
     * @return Returns true if the state matches; false otherwise.
     */
    [[nodiscard]] bool CallIfMatch(const State state, T data) const {
      if (state != state_) {
        return false;
      }

      if (action_) {
        action_(data);
      }

      return true;
    }
  };

  // Forward declaration of the state machine.
  template<typename T>
  class Fsm;

  /**
   * A class used to store a child state machine and the owning state.
   */
  template<typename T>
  class Child {
    //! The owning state.
    const State state_; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The associated state machine.
    Fsm<T>& fsm_; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

  public:
    /**
     * Initializes a new instance of the @see Child class.
     * @param state The state which is the owner of this child.
     * @param fsm A reference to the child fsm to store.
     */
    constexpr Child(const State state, Fsm<T>& fsm) : state_ { state }, fsm_ { fsm } {}

    /**
     * Gets a value indicating whether the stored state is equals to the given state.
     * @return Returns true if the state matches; false otherwise.
     */
    [[nodiscard]] bool Match(const State state) const {
      return state == state_;
    }

    /**
     * Gets a value indicating whether the stored state machine has reached the final state.
     * @return Returns true if the state machine has reached the final state; false otherwise.
     */
    [[nodiscard]] bool HasFinished() const {
      return fsm_.HasFinished();
    }

    /**
     * Starts the state machine, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param data The data provided to the underlaying action handler.
     */
    void Start(const State state, T data) {
      if (!Match(state)) {
        return;
      }

      fsm_.Start(data);
    }

    /**
     * Triggers the state machine, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param event The event to forward to the state machine.
     * @param data The data provided to the underlaying action handler.
     * @return Returns true if the event was handled; false otherwise.
     */
    bool Trigger(const State state, const Event event, T data) {
      if (!Match(state) || HasFinished()) {
        return false;
      }

      return fsm_.Trigger(event, data);
    }

    /**
     * Calls the do handler on the current state of the state machine, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param data The data provided to the underlaying action handler.
     */
    void Do(const State state, T data) const {
      if (!Match(state) || HasFinished()) {
        return;
      }

      fsm_.Do(data);
    }

    /**
     * Activates the current state of the state machine, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param data The data provided to the underlaying action handler.
     * @param history The history to use at the activation.
     */
    void ActivateCurrentState(const State state, T data, const History history) const {
      if (!Match(state) || HasFinished()) {
        return;
      }

      fsm_.ActivateCurrentState(data, history);
    }

    /**
     * Registers a callback function for the embedded state machine.
     * @param on_state_changed The function to register.
     */
    void RegisterStateChangedHandler(std::function<void(const char*, State, State)> on_state_changed) {
      fsm_.RegisterStateChangedHandler(on_state_changed);
    }

    /**
     * Calls dump on the current state of the state machine, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param level The level to provide to the output callback.
     * @param state_callback A callback function which will be called for every state.
     */
    void Dump(const State state, const int level, const std::function<void(int, const char*, int)>& state_callback)
        const {
      if (!Match(state)) {
        return;
      }

      fsm_.Dump(level, state_callback);
    }

    /**
     * Calls dump on the current state of the state machine, if the stored state matches the given state.
     * @param state The state which asks for an action.
     * @param stream An output stream to write the output to.
     */
    void Dump(const State state, std::ostream& stream) const {
      if (!Match(state)) {
        return;
      }

      fsm_.Dump(stream);
    }
  };

  /**
   * A class to store data regarding to a state change.
   */
  template<typename T>
  struct ChangeStateData {
    //! A value indicating whether an event was handled.
    const bool handled {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! A pointer to the state to change to.
    const State to {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    //! The information whether to switch to a history state.
    const History history {}; // NOLINT(cppcoreguidelines-avoid-const-or-ref-data-members)

    /**
     * Initializes a new instance of the @see ChangeStateData class.
     * @param was_handled A value indicating whether an event was handled.
     * @param state_to A pointer to the state to change to.
     * @param history_type The information whether to switch to a history state.
     */
    constexpr ChangeStateData(const bool was_handled, const State state_to, const History history_type) :
        handled { was_handled }, to { state_to }, history { history_type } {}

    /**
     * Initializes a new instance of the @see ChangeStateData class.
     * @param was_handled A value indicating whether an event was handled.
     */
    constexpr explicit ChangeStateData(const bool was_handled) : handled { was_handled } {}
  };
} // namespace fsm
