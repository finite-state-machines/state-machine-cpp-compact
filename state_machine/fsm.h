/*
  Copyright (c) 20016-2023 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#pragma once

#include <ostream>

#include "array_ptr.h"
#include "data.h"

#include <algorithm>

namespace fsm {

  /**
   * Class managing the states of a synchronous FSM (finite state machine).
   * @tparam T The type of data provided to the condition and action handlers.
   */
  template<typename T>
  class Fsm {
    //! The name of the state machine.
    const char* name_;

    //! Holds the actual state of the state machine.
    State current_state_;

    //! An array to store the transition information.
    ArrayPtr<Transition<T>> transitions_ {};

    //! An array to store the child machines.
    ArrayPtr<Child<T>> children_ {};

    //! The handler method for the states entry actions.
    ArrayPtr<Action<T>> entry_actions_ {};

    //! The handler method for the reactions in the state.
    ArrayPtr<Action<T>> do_actions_ {};

    //! The handler method for the states exit actions.
    ArrayPtr<Action<T>> exit_actions_ {};

    std::function<void(const char*, State, State)> on_state_changed_ {};

  public:
    /**
     * Initializes a new instance of the Fsm class.
     * @param name The name of the FSM.
     */
    explicit Fsm(const char* name) : name_ { name }, current_state_ { kInitialState } {}

    /**
     * Gets the currently active state.
     */
    [[nodiscard]] State GetCurrentState() const {
      return current_state_;
    }

    /**
     * Gets a value indicating whether the automaton has started.
     */
    [[nodiscard]] bool IsRunning() const {
      return current_state_ != kInitialState && !HasFinished();
    }

    /**
     * Gets a value indicating whether the automaton has reached the final state.
     */
    [[nodiscard]] bool HasFinished() const {
      return current_state_ == kFinalState;
    }

    /**
     * Gets the name of the state machine.
     */
    [[nodiscard]] const char* Name() const {
      return name_;
    }

    /**
     * Adds the transitions to this state machine.
     * @param transitions An array containing the transitions.
     * @return Returns a reference to this to allow chaining.
     */
    Fsm& Transitions(const ArrayPtr<Transition<T>> transitions) {
      transitions_ = transitions;
      return *this;
    }

    /**
     * Adds the child machines to this state machine.
     * @param children An array containing the child machines.
     * @return Returns a reference to this to allow chaining.
     */
    Fsm& Children(const ArrayPtr<Child<T>> children) {
      children_ = children;
      return *this;
    }

    /**
     * Adds the entry actions to this state machine.
     * @param actions An array containing the actions.
     * @return Returns a reference to this to allow chaining.
     */
    Fsm<T>& EntryActions(const ArrayPtr<Action<T>> actions) {
      entry_actions_ = actions;
      return *this;
    }

    /**
     * Adds the do actions to this state machine.
     * @param actions An array containing the actions.
     * @return Returns a reference to this to allow chaining.
     */
    Fsm<T>& DoActions(const ArrayPtr<Action<T>> actions) {
      do_actions_ = actions;
      return *this;
    }

    /**
     * Adds the exit actions to this state machine.
     * @param actions An array containing the actions.
     * @return Returns a reference to this to allow chaining.
     */
    Fsm<T>& ExitActions(const ArrayPtr<Action<T>> actions) {
      exit_actions_ = actions;
      return *this;
    }

    /**
     * Starts the behavior of the Fsm class. Executes the transition from the
     * start state to the first user defined state.
     * @remarks This method calls the initial states OnEntry method.
     * @param data The data object.
     */
    void Start(T data) {
      SetState(kInitialState);
      Trigger(kStartEvent, data);
    }

    /**
     * Sets the provided state as active state and starts child machines if present, e.g. to resume after a reboot.
     * @remarks This method does not call the entry function of the state.
     * @param state The state to set as current state.
     * @param data The data object.
     */
    void Resume(const State state, T data) {
      SetState(state);
      StartChildren(data);
    }

    /**
     * This method is not intended for normal use. Sets the provided state as active state.
     * @remarks Use this method for testing purposes or to recover from a reboot.
     * @param state The state to set as current state.
     */
    void SetState(const State state) {
      const auto state_before = current_state_;
      current_state_ = state;
      ReportStateChange(state_before, current_state_);
    }

    /**
     * Registers a callback function for this machine and all children which will be called
     * every time a state changes.
     * @param on_state_changed The function to register.
     */
    void RegisterStateChangedHandler(std::function<void(const char*, State, State)> on_state_changed) {
      on_state_changed_ = on_state_changed;

      for (auto& child : children_) {
        child.RegisterStateChangedHandler(on_state_changed);
      }
    }

    /**
     * Triggers a transition.
     * @param trigger The event occurred.
     * @param data The data provided to the condition and action handlers.
     * @returns Returns true if the event was handled, false otherwise.
     */
    bool Trigger(const Event trigger, T data) {
      if (trigger == kNoEvent) {
        return false;
      }

      auto effective_trigger = trigger;
      if (ProcessChildren(&effective_trigger, data)) {
        return true;
      }

      const auto handled_data = ProcessTransitions(effective_trigger, data);
      if (handled_data.to == kInvalidState) {
        return false;
      }

      SetState(handled_data.to);
      ActivateCurrentState(data, handled_data.history);

      return handled_data.handled;
    }

    /**
     * Triggers the reaction in the state.
     * @param data The data provided to the condition and action handlers.
     */
    void Do(T data) const {
      DoChildren(data);
      FireDo(data);
    }

    /**
     * Reports the machine name and the current state and, if available, the names and current states of all sub machines.
     * @param state_callback A callback function which will be called for every state. The first parameter is the level in
     * the hierarchy of the state machines (the same level can be occur multiple times if there are concurrent sub
     * states). The second parameter is the name of the state machine and the third one is the id of the state.
     */
    void Dump(const std::function<void(int, const char*, int)>& state_callback) const {
      Dump(0, state_callback);
    }

    /**
     * Reports the machine name and the current state and, if available, the names and current states of all sub machines.
     * @param level The level to provide to the output callback.
     * @param state_callback A callback function which will be called for every state. The first parameter is the level in
     * the hierarchy of the state machines (the same level can be occur multiple times if there are concurrent sub
     * states). The second parameter is the name of the state machine and the third one is the id of the state.
     */
    void Dump(const int level, const std::function<void(int, const char*, int)>& state_callback) const {
      if (!state_callback) {
        return;
      }

      state_callback(level, name_, current_state_.Id());

      for (auto& child : children_) {
        child.Dump(current_state_, level + 1, state_callback);
      }
    }

    /**
     * Prints the machine name and the current state and, if available, the names and current states of all sub machines.
     * @param stream An output stream to write the output to.
     */
    void Dump(std::ostream& stream) const {
      stream << "{\"" << name_ << R"(":{"state":)" << current_state_.Id();

      if (children_.length() != 0) {
        stream << R"(,"children":[)";
        for (const auto& child : children_) {
          child.Dump(current_state_, stream);
          stream << ",";
        }

        stream.seekp(-1, std::ios_base::end);
        stream << "]";
      }

      stream << "}}";
    }

    /**
     * Activates the new state.
     * Calls the OnEntry handler of this state and starts all child FSMs if there
     * are some.
     * @param data The data object.
     * @param history A value, containing the information which history model has
     * to be used.
     */
    void ActivateCurrentState(T data, const History history) {
      if (history.IsHistory() && TryStartHistory(data)) {
        return;
      }

      if (history.IsDeepHistory() && TryStartDeepHistory()) {
        return;
      }

      FireOnEntry(data);
      StartChildren(data);
    }

  private:
    /**
     * Processes the transitions.
     * @param trigger The event occurred.
     * @param data The data provided to the condition and action handlers.
     * @returns Returns data indicating whether the event was handled and needed
     * to proceed with the new state.
     */
    ChangeStateData<T> ProcessTransitions(const Event trigger, T data) const {
      for (const auto& transition : transitions_) {
        if (transition.Match(current_state_, trigger) && transition.Condition(data)) {
          return LeaveState(transition, data);
        }
      }

      return ChangeStateData<T> { false };
    }

    /**
     * Changes the state to the one stored in the transition object.
     * @param transition The actual transition.
     * @param data The data provided to the condition and action handlers.
     * @returns Returns the data needed to proceed with the new state.
     */
    ChangeStateData<T> LeaveState(const Transition<T>& transition, T data) const {
      FireOnExit(data);

      ChangeStateData<T> result { !transition.IsToFinal(), transition.To(), transition.GetHistory() };
      return result;
    }

    /**
     * Let all direct child FSMs continue working. Returns false if there is no active child.
     * If this method returns false, @see Start has to be called to complete the operation.
     * @param data The data object.
     * @return Returns true if there are active children; false otherwise.
     */
    bool TryStartHistory(T data) {
      if (!HasActiveChildren()) {
        return false;
      }

      for (auto& child : children_) {
        child.ActivateCurrentState(current_state_, data, History {});
      }

      return true;
    }

    /**
     * Starts all registered sub state machines.
     * @param data The data object.
     */
    void StartChildren(T data) {
      for (auto& child : children_) {
        child.Start(current_state_, data);
      }
    }

    /**
     * Gets a value indicating whether this state has active child machines.
     */
    [[nodiscard]] bool HasActiveChildren() const {
      return std::any_of(children_.begin(), children_.end(), [this](const auto& child) {
        return child.Match(current_state_) && !child.HasFinished();
      });
    }

    /**
     * Let all child FSMs continue working. Returns false if there is no active child.
     * If this method returns false, @see Start has to be called to complete the operation.
     * @return Returns true if there are active children; false otherwise.
     */
    [[nodiscard]] bool TryStartDeepHistory() const {
      return HasActiveChildren();
    }

    /**
     * Processes the child machines.
     * @remarks If the last child machine went to the final state, the parameter
     *          "trigger" is changed to @see FsmEvent::NoEvent and the method
     *          returns false.
     * @param trigger The event occurred.
     * @param data The data provided to the condition and action handlers.
     * @returns Returns true if the event was handled, false otherwise.
     */
    bool ProcessChildren(Event* trigger, T data) {
      if (!HasActiveChildren()) {
        return false;
      }

      const auto handled = TriggerChildren(*trigger, data);
      if (handled) {
        return true;
      }

      if (!HasActiveChildren()) {
        *trigger = kNoEvent;
      }

      return false;
    }

    /**
     * Triggers the child machines.
     * @param trigger The event occurred.
     * @param data The data provided to the condition and action handlers.
     * @returns Returns true if the event was handled, false otherwise.
     */
    bool TriggerChildren(const Event trigger, T data) {
      auto handled = false;
      for (auto& child : children_) {
        handled |= child.Trigger(current_state_, trigger, data);
      }

      return handled;
    }

    /**
     * Executes the Do action on the child machines.
     * @param data The data provided to the condition and action handlers.
     */
    void DoChildren(T data) const {
      for (const auto& child : children_) {
        child.Do(current_state_, data);
      }
    }

    /**
     * Fires the OnEntry event.
     * @param data The data object.
     */
    void FireOnEntry(T data) const {
      Fire(entry_actions_, data);
    }

    /**
     * Fires the OnExit event.
     * @param data The data object.
     */
    void FireOnExit(T data) const {
      Fire(exit_actions_, data);
    }

    /**
     * Fires the Do event.
     * @param data The data object.
     */
    void FireDo(T data) const {
      Fire(do_actions_, data);
    }

    /**
     * Calls one of the actions of the state
     * @param actions A list with all actions to search for the right one.
     * @param data The data object.
     */
    template<typename TContainer>
    void Fire(const TContainer& actions, T data) const {
      for (const auto& action : actions) {
        if (action.CallIfMatch(current_state_, data)) {
          return;
        }
      }
    }

    /**
     * Reports a state change if there is a callback registered
     * @param from The state before the change.
     * @param to The state after the change.
     */
    void ReportStateChange(const State from, const State to) const {
      if (!on_state_changed_) {
        return;
      }

      on_state_changed_(name_, from, to);
    }
  };

} // namespace fsm
