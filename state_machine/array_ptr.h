/*
  Copyright (c) 20016-2023 Frank Listing

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#pragma once

#include <array>

// ReSharper disable CppInconsistentNaming

/**
 * A class to encapsulate a C array or the raw data of a std::array.
 */
template<typename T>
class ArrayPtr {
  //! The pointer to the begin of the array.
  T* pointer_ {};

  //! The number of items contained in the array.
  size_t length_ {};

public:
  /**
   * Initializes a new instance of the ArrayPtr class.
   */
  ArrayPtr() = default;

  /**
   * Initializes a new instance of the ArrayPtr class.
   * Determines the values for the member variables from the given array.
   * @param array The array to encapsulate.
   * @remark Implicite conversion is wanted! Not explicit to have an easier state machine definition.
   */
  template<typename Ty, size_t Size>
  ArrayPtr(Ty (&array)[Size]) : ArrayPtr(array, Size) {}

  /**
   * Initializes a new instance of the ArrayPtr class.
   * @param pointer The pointer to the begin of the array.
   * @param length The number of items contained in the array.
   */
  ArrayPtr(T* pointer, const size_t length) : pointer_ { pointer }, length_ { length } {}

  /**
   * Initializes a new instance of the ArrayPtr class.
   * @param array The std::array to encapsulate.
   * @remark Implicite conversion is wanted! Not explicit to have an easier state machine definition.
   */
  template<typename Ty, std::size_t Size>
  ArrayPtr(std::array<Ty, Size>& array) : pointer_ { array.data() }, length_ { array.size() } {}

  /**
   * Gets the number of items contained in the array.
   */
  [[nodiscard]] size_t length() const {
    return length_;
  }

  /**
   * Get an iterator to the first element.
   */
  T* begin() {
    return pointer_;
  }

  /**
   * Get a const iterator to the first element.
   */
  [[nodiscard]] const T* begin() const {
    return pointer_;
  }

  /**
   * Get an iterator to the position after the last element.
   */
  T* end() {
    return pointer_ + length_;
  }

  /**
   * Get a const iterator to the position after the last element.
   */
  [[nodiscard]] const T* end() const {
    return pointer_ + length_;
  }

  /**
   * Allows the access to the arrays items.
   * @param index The index of the item to access.
   */
  const T& operator[](const int index) const {
    return pointer_[index];
  }
};
