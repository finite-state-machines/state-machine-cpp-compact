#include "traffic_light_controller.h"

using namespace fsm;

/**
 * Initializes a new instance of the TrafficLight class.
 */
TrafficLightController::TrafficLightController()
    // clang-format off
    : main_controller_{"main"},
      day_controller_{"Day_Controller"},
      night_controller_{"Night_Controller"},
      main_transitions_{
        Transition<TrafficLight>{kInitialState, kStartEvent, kNightMode},
        Transition<TrafficLight>{kNightMode, kNoEvent, kDayMode},
        Transition<TrafficLight>{kDayMode, kNoEvent, kNightMode}},
      main_children_{
        Child<TrafficLight>{kNightMode, night_controller_},
        Child<TrafficLight>{kDayMode, day_controller_}},
      day_transitions_{
        Transition<TrafficLight>{kInitialState, kStartEvent, kDayShowingRed},
        Transition<TrafficLight>{kDayShowingRed, kTimerEvent, Not([](auto tl) { return tl.IsNightMode(); }), kDayShowingRedYellow},
        Transition<TrafficLight>{kDayShowingRed, kTimerEvent, [](auto tl) { return tl.IsNightMode(); }, kFinalState},
        Transition<TrafficLight>{kDayShowingYellow, kTimerEvent, kDayShowingRed},
        Transition<TrafficLight>{kDayShowingRedYellow, kTimerEvent, kDayShowingGreen},
        Transition<TrafficLight>{kDayShowingGreen, kTimerEvent, kDayShowingYellow}
      },
      day_entry_actions_{
        Action<TrafficLight>{kDayShowingRed, [](auto tl) { tl.SetLamps(true, false, false); }},
        Action<TrafficLight>{kDayShowingYellow, [](auto tl) { tl.SetLamps(false, true, false); }},
        Action<TrafficLight>{kDayShowingRedYellow, [](auto tl) { tl.SetLamps(true, true, false); }},
        Action<TrafficLight>{kDayShowingGreen, [](auto tl) { tl.SetLamps(false, false, true); }}},
      night_transitions_{
        Transition<TrafficLight>{kInitialState, kStartEvent, kNightShowingYellow},
        Transition<TrafficLight>{kNightShowingYellow, kTimerEvent, [](auto tl) { return tl.IsNightMode(); }, kNightShowingNothing},
        Transition<TrafficLight>{kNightShowingYellow, kTimerEvent, Not([](auto tl) { return tl.IsNightMode(); }), kFinalState},
        Transition<TrafficLight>{kNightShowingNothing, kTimerEvent, kNightShowingYellow}
      },
      night_entry_actions_{
        Action<TrafficLight>{kNightShowingYellow, [](auto tl) { tl.SetLamps(false, true, false); }},
        Action<TrafficLight>{kNightShowingNothing, [](auto tl) { tl.SetLamps(false, false, false); }},
          // clang-format on
      } {
  main_controller_.Transitions(main_transitions_).Children(main_children_);
  day_controller_.Transitions(day_transitions_).EntryActions(day_entry_actions_);
  night_controller_.Transitions(night_transitions_).EntryActions(night_entry_actions_);

  StartBehavior();
}

/**
 * Sends the timer event to the main controller.
 */
void TrafficLightController::SendTimerEvent() {
  main_controller_.Trigger(kTimerEvent, traffic_light_);
}

/**
 * Toggles between day mode and night mode.
 */
void TrafficLightController::SwitchMode() {
  traffic_light_.SetNightMode(!traffic_light_.IsNightMode());
}

void TrafficLightController::StartBehavior() {
  main_controller_.Start(traffic_light_);
}
