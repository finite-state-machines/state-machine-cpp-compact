#include <cstdio>

#include "traffic_light_controller.h"

/**
 * Removes the rest of the line from the input bufffer.
 */
void ClearInputBuffer() noexcept {
  int c;
  while ((c = getchar()) != '\n' && c != EOF) {
  }
}

/**
 * Reads one character from the command line.
 * @param default_value The character to return if the line is empty.
 * @return Returns the first char from the command line or the @see default_value if the line is empty.
 */
char GetSingleCharacter(const char default_value) noexcept {
  const auto c = getchar();

  if (c == '\n' || c == EOF) {
    return default_value;
  }

  ClearInputBuffer();
  return static_cast<char>(c);
}

/**
 * Reads the user commands from the console input and forwards it to the state machine.
 * @param traffic_light The state machine to trigger.
 */
bool ProcessInput(TrafficLightController& traffic_light) noexcept {
  const auto input = GetSingleCharacter('t');
  if (input == 'q' || input == 'Q') {
    return false;
  }

  if (input == 'b' || input == 'B') {
    traffic_light.SwitchMode();
    return true;
  }

  traffic_light.SendTimerEvent();
  return true;
}

int main() noexcept {
  TrafficLightController traffic_light;

  while (ProcessInput(traffic_light)) {
  }
}
