#ifndef TRAFFIC_LIGHT_CONTROLLER_H
#define TRAFFIC_LIGHT_CONTROLLER_H

#include "fsm.h"
#include "traffic_light.h"

/**
 * The event used to trigger the state machines.
 */
constexpr fsm::Event kTimerEvent{'t'};

/**
 * A class controlling the traffic light.
 */
class TrafficLightController {
  //! The state machine on the top
  fsm::Fsm<TrafficLight> main_controller_;
  static constexpr fsm::State kNightMode{11};
  static constexpr fsm::State kDayMode{12};

  //! The state machine handling the day working mode of the traffic light.
  fsm::Fsm<TrafficLight> day_controller_;
  static constexpr fsm::State kDayShowingRed{21};
  static constexpr fsm::State kDayShowingRedYellow{22};
  static constexpr fsm::State kDayShowingYellow{23};
  static constexpr fsm::State kDayShowingGreen{24};

  //! The state machine handling the night mode of the traffic light.
  fsm::Fsm<TrafficLight> night_controller_;
  static constexpr fsm::State kNightShowingYellow{31};
  static constexpr fsm::State kNightShowingNothing{32};

  //! An array storing the transitions of the main machine.
  fsm::Transition<TrafficLight> main_transitions_[3];

  //! An array storing the child machines of the main machine.
  fsm::Child<TrafficLight> main_children_[2];

  //! An array storing the transitions of the machine handling the day mode.
  fsm::Transition<TrafficLight> day_transitions_[6];

  //! An array storing the entry actions of the machine handling the day mode.
  fsm::Action<TrafficLight> day_entry_actions_[4];

  //! An array storing the transitions of the machine handling the night mode.
  fsm::Transition<TrafficLight> night_transitions_[4];

  //! An array storing the entry actions of the machine handling the night mode.
  fsm::Action<TrafficLight> night_entry_actions_[2];

  //! The traffic light object to control
  TrafficLight traffic_light_;

 public:
  /**
   * Initializes a new instance of the TrafficLight class.
   */
  TrafficLightController();

  /**
   * Sends the timer event to the main controller.
   */
  void SendTimerEvent();

  /**
   * Toggles between day mode and night mode.
   */
  void SwitchMode();

 private:
  void StartBehavior();
};

#endif  // TRAFFIC_LIGHT_CONTROLLER_H
