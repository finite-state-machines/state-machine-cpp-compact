$param = $args[0]
if ( [System.String]::IsNullOrWhiteSpace($param)) {
    Write-Output "Please provide a command"
    Write-Output "usage: ./run init || build || test || all"
    exit 1
}

try {
    if ( $param.Equals("all") -or $param.Equals("init")) {
        New-Item -Name "build" -ItemType "directory" -Force
    }

    Set-Location "build" > null 2>&1
    if (-not $?) {
        throw "Directory 'build' does not exist. Run 'init' first"
    }

    try {
        if ( $param.Equals("test")) {
            ctest .
        }
        elseif ( $param.Equals("build")) {
            cmake --build .
        }
        elseif ( $param.Equals("init")) {
            cmake  ..
        }
        elseif ( $param.Equals("all")) {
            cmake  ..
            cmake --build .
            ctest .
        }
        else {
            Write-Output "wrong target"
        }
    }
    finally {
        Set-Location ..
    }
}
catch {
    Write-Output "!-----------------------------------------------------------------------"
    Write-Output "!   $( $_.Exception.Message )"
    Write-Output "!-----------------------------------------------------------------------"
    exit 1
}
